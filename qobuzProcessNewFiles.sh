#!/bin/bash
# @TODO process all .tar files in Music and loop on them
EXIT_CODE=0
MUSIC_ARCHIVE_FILE=$1
NEW_MUSIC_FOLDER=/home/didier/Music
FILE_NEW_MUSIC=/home/didier/bin/fileNewMusic.sh
FILE_TYPES=(ogg flac)

displayUsage()
{
    echo "Usage: $0 <archive.tar>"
}

if [[ $# -ne 1 ]]; then
    displayUsage
    exit 1
fi

cd $NEW_MUSIC_FOLDER
#rm -rf TempRaw
#mkdir TempRaw
#mv $MUSIC_ARCHIVE_FILE TempRaw/
cd TempRaw
#tar -xvf $MUSIC_ARCHIVE_FILE
#oggenc -q 6 */**.flac
# Ignore first result (the current directory itself)
FOLDERS=$(find . -type d | grep -vie "\.$")
for FOLDER in $FOLDERS; do
    cd $FOLDER
    pwd
    # This script only works with folder structure created with EasyTag.
    #$FILE_NEW_MUSIC
    for FILE_TYPE in "${FILE_TYPES[@]}"; do
        FILES=$(find . -iname "*.$FILE_TYPE")
        for FILE in "${FILES[@]}"; do
            echo "$FILE";
        done
    done
    # read artist, year-album tags of first file
    #   if no tag, prompt user
    # create folders in Lossy and Lossless
    # read track number-track name
    # move/rename file into created folders
    cd ..
done

exit $EXIT_CODE
