#!/bin/bash
# Move the file currently playing by cmus into the trash.
#
# Write trash info file so it can be properly restored.
# Display a notification.
FILE_TO_DELETE=
MESSAGE=
URGENCY=low
TIMEOUT=4000
TRASH=~/.local/share/Trash
EXIT_CODE=

function moveToTrash()
{
    TRASH_INFO_FILE=$TRASH/info/$(basename "$1").trashinfo
    mv "$1" $TRASH/files/
    EXIT_CODE=$?
    echo "[Trash Info]" > "$TRASH_INFO_FILE"
    echo "Path=$1" >> "$TRASH_INFO_FILE"
    return $EXIT_CODE
}

if [[ $(cmus-remote -Q | grep 'file') ]]; then
    FILE_TO_DELETE="$(cmus-remote -Q | grep -i 'file' | sed 's/file //')"
    moveToTrash "$FILE_TO_DELETE"
    EXIT_CODE=$?
    if [[ $? == 0 ]]; then
        MESSAGE="$FILE_TO_DELETE has been put to trash."
    else
        MESSAGE="Error: $FILE_TO_DELETE has NOT been put to trash."
        URGENCY=critical
    fi
    notify-send -a $(basename "$0") -u $URGENCY -t $TIMEOUT "$MESSAGE"
fi
exit $EXIT_CODE
