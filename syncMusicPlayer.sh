#!/bin/bash
# TODO remove picture tag from OGG due to player bug freezing on OGG file having such tag
# Sync files on my music player, using the content of files.sync.

USER=didier
DESKTOP_ENVIRONMENT=i3
CODE_EXIT=0
MESSAGE="No message set"
URGENCY=low
TIMEOUT=4000
#PATH_CURRENT=$(pwd)
# Clip Sport UUID
UUID_MAIN="68FF-90AB"
# Memory card UUID
#UUID_CARD="3538-6661"
DEVICE_NAME=
MOUNTPOINT_MAIN=/home/didier/MountpointMusicPlayer
DEVICE_FILE_MAIN=/dev/disk/by-uuid/$UUID_MAIN
FILE_SYNC="/home/didier/Music/Clip Sport/files.sync"
MUSIC_FOLDER_PLAYER="$MOUNTPOINT_MAIN/Music"
TEMP_FOLDER="/tmp/MusicSync"

function player_mount()
{
    if [[ -b $DEVICE_FILE_MAIN ]]; then
        echo "$DEVICE_FILE_MAIN found."
        CODE_EXIT=0
    else
        MESSAGE="Error: $DEVICE_FILE_MAIN NOT found. Is it connected ?"
        echo $MESSAGE
        URGENCY=critical
        CODE_EXIT=1
    fi

    if [[ $CODE_EXIT == 0 ]]; then
        player_unmount
        if [[ $? == 0 ]]; then
            mkdir "$MOUNTPOINT_MAIN"
            CODE_EXIT=$?
            if [[ $CODE_EXIT == 0 ]]; then
                echo "$MOUNTPOINT_MAIN has been created."
            else
                MESSAGE="Error: could not create ${MOUNTPOINT_MAIN}."
                URGENCY=critical
                echo $MESSAGE
            fi
        fi
    fi

    if [[ $CODE_EXIT == 0 ]]; then
        mount "$DEVICE_FILE_MAIN" "$MOUNTPOINT_MAIN"
        CODE_EXIT=$?
        if [[ $CODE_EXIT == 0 ]]; then
            echo "$DEVICE_FILE_MAIN has been mounted in $MOUNTPOINT_MAIN".
            # Grab "Clip Sport" from /dev/sdc: LABEL="Clip Sport" UUID="68FF-90AB" TYPE="vfat"
            # @source /dev/sdc: LABEL="Clip Sport" UUID="68FF-90AB" TYPE="vfat"
            DEVICE_NAME=$(blkid | grep $UUID_MAIN | awk 'BEGIN{FS="\""} {print $2}')
        else
            MESSAGE="Error: could not mount $DEVICE_FILE_MAIN in ${MOUNTPOINT_MAIN}. Are you root ?"
            URGENCY=critical
            echo $MESSAGE
        fi
    fi
}

# Remove files that are not presents in $FILE_SYNC
function deleteOldFiles()
{
    cd "$MUSIC_FOLDER_PLAYER"
    for f in *.{flac,mp3,ogg}; do
        grep -F --quiet "$f" "$FILE_SYNC"
        if [[ $? -eq 1 ]]; then
            echo "Deleting $f..."
            rm -f "$f"
        fi
    done
    cd -
}

function player_sync()
{
    echo "Starting sync..."
    # --delete does not work with --files-from
    # Make sure destination exists.
    mkdir -p "$MUSIC_FOLDER_PLAYER"
    deleteOldFiles
    # Source is / because I use absolute paths in the sync file.
    # --no-relative tells rsync to not preserve source hierarchy
    # @source http://stackoverflow.com/a/9766681
    rsync -vhuz --stats --progress --files-from="$FILE_SYNC" --no-relative / "$MUSIC_FOLDER_PLAYER"
    CODE_EXIT=$?
    IS_ERROR=0
    if [[ $CODE_EXIT > 0 ]]; then
        MESSAGE="Error: rsync exited with code ${CODE_EXIT}."
        IS_ERROR=1
        echo "$MESSAGE"
    fi
    if [[ $IS_ERROR == 0 ]]; then
        MESSAGE="$DEVICE_NAME has been successfully updated."
    else
        MESSAGE="Error: an error occured while syncing data."
        URGENCY=critical
    fi
    echo "$MESSAGE"
}
function player_unmount()
{
    mountpoint "$MOUNTPOINT_MAIN"
    if [[ $? == 0 ]]; then
        sync
        umount "$MOUNTPOINT_MAIN"
        CODE_EXIT=$?
        if [[ $CODE_EXIT == 0 ]]; then
            echo "${MOUNTPOINT_MAIN} has been unmounted."
        else
            MESSAGE="Error: could not unmount ${MOUNTPOINT_MAIN}. Are you root ?"
            URGENCY=critical
            echo $MESSAGE
        fi
    else
        echo "$MOUNTPOINT_MAIN is not mounted."
    fi

    if [[ $CODE_EXIT == 0 ]]; then
        if [[ -d "$MOUNTPOINT_MAIN" ]]; then
            rmdir "$MOUNTPOINT_MAIN"
            CODE_EXIT=$?
            if [[ $CODE_EXIT == 0 ]]; then
                echo "Directory ${MOUNTPOINT_MAIN} has been removed."
            else
                MESSAGE="Error: could not remove directory ${MOUNTPOINT_MAIN}."
                URGENCY=critical
                echo $MESSAGE
            fi
        else
            echo "Directory $MOUNTPOINT_MAIN does not exist."
        fi
    fi
    return $CODE_EXIT
}
# Dbus session is missing when run as root, so grab it first.
# This use the pid of my window manager.
# @source https://www.eanderalx.org/linux/notify_send_cron
init_notify() {
    sudo grep -z DBUS_SESSION_BUS_ADDRESS /proc/$(/bin/pidof "$DESKTOP_ENVIRONMENT")/environ | cut -d "=" -f2-4 > /tmp/dbus_session_id
}

init_notify
sudo -u $USER DISPLAY=:0.0 DBUS_SESSION_BUS_ADDRESS=$(head /tmp/dbus_session_id) /usr/bin/notify-send -a $(basename "$0") -u $URGENCY -t $TIMEOUT "Starting..."
player_mount
if [[ $CODE_EXIT == 0 ]]; then
    player_sync
    player_unmount
fi
#cd $PATH_CURRENT
echo "Task finished with exit code ${CODE_EXIT}."
sudo -u $USER DISPLAY=:0.0 DBUS_SESSION_BUS_ADDRESS=$(head /tmp/dbus_session_id) /usr/bin/notify-send -a $(basename "$0") -u $URGENCY -t $TIMEOUT "$MESSAGE"
exit $CODE_EXIT
