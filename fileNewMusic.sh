#!/bin/bash

# fileNewMusic v0.1
# When I download music, first I check/correct tags and filenames.
# Using the following structure: AlbumArtist/Year-Album/Track-Title.extension
# Then, I convert FLAC files to OGG Vorbis.
# 
# Goal for this script:
# I want to move OGG/MP3 files to Music/Letter/[above structure].
# I want to move FLAC files to Music (FLAC)/Letter/[above structure].
# I want to copy covers to both destinations.

# PLAN
# @todo check if there are FLAC files before creating new folders
# @todo process multiple albums
# User does cd to Downloads/[downloaded album]
# Get artist name from folder
# Get album name from subfolder
# get artist initial
# check if initial folder exists
#     if not create initial folder
# check if artist folder exists
#     if not create artist folder
# create album folder
# cp files to album folder

# SCRIPT
# defines home folder
HOME="/home/didier"
LOG="/tmp/fileNewMusic$(date +%Y%m%d-%Hh%M).log"
# defines Music folder
#MUSIC="/home/didier/Music"
#MUSICFLAC="/home/didier/Music (FLAC)"
# get artist from folder name
ARTIST=`pwd | sed 's/.*\///'`
echo "Artist: $ARTIST"
# get first letter of artist name
INITIAL=`echo $ARTIST | cut -c1`
echo "Initial: $INITIAL"
# get albums, which are subfolders of artist
# ls -d produces no output, */ is bash specific and should be replaced by find for interoperability
# sed removes the trailing slash
ALBUMS=`ls -d */ | sed 's/\///g'`
echo "Albums: $ALBUMS"
DESTINATION_PATH=""
copy_music() {
	MUSIC="$HOME/Music/Lossless"
	IS_LOSSY=false
	if [ "$1" = "lossy" ]; then
		MUSIC="$HOME/Music/Lossy"
		IS_LOSSY=true
	fi
	# check if folders exist, if not, create them
	if [ ! -d "$MUSIC/$INITIAL" ]; then
	    mkdir "$MUSIC/$INITIAL"
	fi
	if [ ! -d "$MUSIC/$INITIAL/$ARTIST" ]; then
	    mkdir "$MUSIC/$INITIAL/$ARTIST"
	fi
	if [ ! -d "$MUSIC/$INITIAL/$ARTIST/$ALBUMS" ]; then
	    mkdir "$MUSIC/$INITIAL/$ARTIST/$ALBUMS"
	fi
	DESTINATION_PATH="$MUSIC/$INITIAL/$ARTIST/$ALBUMS"
	
	# moves files to album folder
	echo "Copying audio files and covers to $DESTINATION_PATH..."
	if [ $IS_LOSSY == true ]; then
        echo "find \"$ALBUMS\" -iname '*.ogg' -print0 -o -iname '*.mp3' -print0 | grep -z '' | xargs -0 -I {} cp {} \"$DESTINATION_PATH\"" >> $LOG
		find "$ALBUMS" -iname '*.ogg' -print0 -o -iname '*.mp3' -print0 | xargs -0 -I {} cp {} "$DESTINATION_PATH"
		#find "$ALBUMS" -iname '*.ogg' -print0 -o -iname '*.mp3' -print0 | grep -z '' | xargs -0 -I {} cp {} "$DESTINATION_PATH"
#		                              ^^^^^^^ ^^                               ^^
#		-print0 allows handling whitespaces or quotes
#		-o or, unfortunately I need to write specific commands (as -print0, -exec) inside each part of the global expression
#		-z replaces new line by a null character, without this, cp doesn't work
	else
		find "$ALBUMS" -iname '*.flac' -print0 | xargs -0 -I {} cp {} "$DESTINATION_PATH"
	fi

	# copies pictures to album folder
	find "`pwd`" -iname '*.jpg' -print0 -o -iname '*.jpeg' -print0 -o -iname '*.png' -print0 -o -iname '*.pdf' -print0 | xargs -0 -I {} cp {} "$DESTINATION_PATH"
	echo "Done."

	# lists the result
	echo "Content of $DESTINATION_PATH:"
	ls "$DESTINATION_PATH"
}
copy_music "lossy"
copy_music

exit 0
