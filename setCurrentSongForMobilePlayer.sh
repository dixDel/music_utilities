#!/bin/bash
# Write the path of the file currently playing into a file containing the list of
# the music files to sync to my mobile music player.
#
# Create the sync file if it does not exist.
# Check if the music file is already in the sync file.
#
# @todo use custom codes instead of passing those of external tools
# Exit codes:
#   - 0: OK
#   - 1001: Entry already present in sync file. Nothing to do.
#
FILE_FOR_MOBILE=$1
SYNC_FILE=~/Music/Clip\ Sport/files.sync
MESSAGE=
URGENCY=low
TIMEOUT=4000
EXIT_CODE=0

# display message to user
#   - exit code
#   - message to display
#   - suggestion to solve error
function message()
{
    EXIT_CODE=$1
    if [[ $EXIT_CODE == 0 ]]; then
        echo "[Success] $2"
    else
        MESSAGE="[Error] $2\n$3"
        URGENCY=critical
        echo $MESSAGE
    fi
    return $EXIT_CODE
}

function createSyncFile()
{
    if [[ ! -f "$SYNC_FILE" ]]; then
        touch "$SYNC_FILE"
        message $? "Create file $SYNC_FILE."
    else
        echo "File $SYNC_FILE exists."
    fi
    return $EXIT_CODE
}

# Set $FILE_FOR_MOBILE.
# If no argument is passed directly to this script,
# call cmus-remote to get a file path.
function getFileForMobile()
{
    if [[ ! $FILE_FOR_MOBILE ]]; then
        echo "No argument given; calling cmus-remote..."
        if [[ $(cmus-remote -Q | grep 'file') ]]; then
            FILE_FOR_MOBILE="$(cmus-remote -Q | grep -i 'file' | sed 's/file //')"
        fi
    fi
    if [[ ! $FILE_FOR_MOBILE ]]; then
        message 1 "Get the file to sync." "Pass me an argument or make sure cmus is running."
    fi
    return $EXIT_CODE
}

# Write $FILE_FOR_MOBILE into $SYNC_FILE.
# Do not create duplicate entry.
function addToSyncFile()
{
    if getFileForMobile == 0; then
        if [[ $(grep -F -c "$FILE_FOR_MOBILE" "$SYNC_FILE") == 0 ]]; then
            echo "$FILE_FOR_MOBILE" >> "$SYNC_FILE"
            message $? "Set $FILE_FOR_MOBILE for sync to mobile player."
        else
            MESSAGE="$FILE_FOR_MOBILE is already in sync file."
            EXIT_CODE=1001
        fi
    fi
    return $EXIT_CODE
}

if createSyncFile == 0; then
    if addToSyncFile == 0; then
        MESSAGE="$FILE_FOR_MOBILE has been set for sync to mobile player."
        sort -fn -o "$SYNC_FILE" "$SYNC_FILE"
    fi
fi

notify-send -a $(basename "$0") -u $URGENCY -t $TIMEOUT "$MESSAGE"

exit $EXIT_CODE
