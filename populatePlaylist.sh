#!/bin/bash
# Add a given file path to a given playlist file (m3u).
#
# Take 2 arguments:
# - playlist name
# - file path
#
# Create playlist file if it does not exist.
#
PATH_DIR_PLAYLIST=~/Music/Playlists
PATH_FILE_PLAYLIST=
FILENAME=
SCRIPT_SET_FOR_SYNC=~/bin/setCurrentSongForMobilePlayer.sh
NB_ARGS=$#
MESSAGE="No message set."
MESSAGE_SUCCESS_DEFAULT="[[FILENAME]] has been added to playlist [[PATH_FILE_PLAYLIST]]."
URGENCY=low
TIMEOUT=4000
EXIT_CODE=0

# check args
function validateArgs()
{
    if [[ $NB_ARGS -ne 2 ]]; then
        MESSAGE="Usage: $(basename $0) playlist_name file_path"
        URGENCY=critical
        echo $MESSAGE
        EXIT_CODE=1
    fi
    return $EXIT_CODE
}

# display message to user
#   - exit code
#   - message to display
function message()
{
    EXIT_CODE=$1
    if [[ $EXIT_CODE == 0 ]]; then
        echo "[Success] $2"
    else
        MESSAGE="[Error] $2"
        URGENCY=critical
        echo $MESSAGE
    fi
    return $EXIT_CODE
}

# create playlist file if it does not exist
function createFile()
{
    PATH_FILE_PLAYLIST="$PATH_DIR_PLAYLIST/${1}.m3u"
    if [[ ! -f $PATH_FILE_PLAYLIST ]]; then
        echo "#EXTM3U" >> "$PATH_FILE_PLAYLIST"
        if message $? "Create ${PATH_FILE_PLAYLIST}." == 0 ; then
            addEmptyLine
        fi
    fi
    return $EXIT_CODE
}

# convert file to DOS format
function convertToDos()
{
    unix2dos "$PATH_FILE_PLAYLIST"
    message $? "Convert $PATH_FILE_PLAYLIST to DOS format."
}

# add path to playlist
# put an empty line at the end of file
# do not write duplicate
function addToPlaylist()
{
    FILENAME=$(basename "$1")
    if [[ $(grep -F -c "$FILENAME" "$PATH_FILE_PLAYLIST") == 0 ]]; then
        # delete last line
        sed -i '$ d' "$PATH_FILE_PLAYLIST"
        if message $? "Delete last line of ${PATH_FILE_PLAYLIST}." == 0; then
            echo "$FILENAME" >> $PATH_FILE_PLAYLIST
            if message $? "Write ${FILENAME} to ${PATH_FILE_PLAYLIST}." == 0; then
                addEmptyLine
            fi
        fi
    else
        message 0 "$FILENAME is already in ${PATH_FILE_PLAYLIST}."
        MESSAGE_SUCCESS_DEFAULT="[[FILENAME]] was already in playlist [[PATH_FILE_PLAYLIST]]."
    fi
    return $EXIT_CODE
}

function addEmptyLine()
{
    echo >> "$PATH_FILE_PLAYLIST"
    message $? "Write empty line at the end of ${PATH_FILE_PLAYLIST}."
    return $?
}

# Call external script
function setPlaylistFileForSync()
{
    "$SCRIPT_SET_FOR_SYNC" "$PATH_FILE_PLAYLIST"
    EXIT_CODE=$?
    if [[ $EXIT_CODE > 1000 ]]; then
        EXIT_CODE=0
    fi
    message $EXIT_CODE "Call external script ${SCRIPT_SET_FOR_SYNC}."
    return $EXIT_CODE
}

if validateArgs "$1" "$2" == 0; then
    if createFile "$1" == 0; then
        if addToPlaylist "$2" == 0; then
            if convertToDos == 0; then
                setPlaylistFileForSync
            fi
        fi
    fi
fi
if [[ $EXIT_CODE == 0 ]]; then
    # ' <<<$MESSAGE_SUCCESS_DEFAULT' replace 'echo $MESSAGE_SUCCESS_DEFAULT |'
    # @todo how to use \1 instead of variable name ?
    MESSAGE=$(
        sed s@"\[\[FILENAME\]\]"@"$FILENAME"@ <<<$MESSAGE_SUCCESS_DEFAULT |
        sed s@"\[\[PATH_FILE_PLAYLIST\]\]"@"$PATH_FILE_PLAYLIST"@
    )
fi
echo $MESSAGE
notify-send -a $(basename "$0") -u $URGENCY -t $TIMEOUT "$MESSAGE"

exit $EXIT_CODE
