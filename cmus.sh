#!/bin/bash
PID_CMUS=$(pgrep cmus)
echo $PID_CMUS
if [ -z $PID_CMUS ]
then
    cmus-remote -u
else
    urxvt -e cmus
fi
