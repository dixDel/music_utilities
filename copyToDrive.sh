#!/bin/bash
# Write the path of the file currently playing into a file containing the list of
# the music files to sync to my mobile music player.
#
# Create the sync file if it does not exist.
# Check if the music file is already in the sync file.
#
# @todo use custom codes instead of passing those of external tools
# Exit codes:
#   - 0: OK
#   - 1001: Entry already present in sync file. Nothing to do.
#
FILE_FOR_MOBILE=$1
DEST_FILENAME=
MESSAGE=
URGENCY=low
TIMEOUT=4000
EXIT_CODE=0

# display message to user
#   - exit code
#   - message to display
#   - suggestion to solve error
function message()
{
    EXIT_CODE=$1
    if [[ $EXIT_CODE == -1 ]]; then
        MESSAGE="[Info] $2"
    elif [[ $EXIT_CODE == 0 ]]; then
        MESSAGE="[Success] $2"
    else
        MESSAGE="[Error] $2\n$3"
        URGENCY=critical
    fi
    echo $MESSAGE
    return $EXIT_CODE
}

# Set $FILE_FOR_MOBILE.
# If no argument is passed directly to this script,
# call cmus-remote to get a file path.
function getFileForMobile()
{
    if [[ ! $FILE_FOR_MOBILE ]]; then
        echo "No argument given; calling cmus-remote..."
        if [[ $(cmus-remote -Q | grep 'file') ]]; then
            FILE_FOR_MOBILE="$(cmus-remote -Q | grep -i 'file' | sed 's/file //')"
        fi
    fi
    if [[ $FILE_FOR_MOBILE ]]; then
        EXIT_CODE=-1
        message $EXIT_CODE "Processing $FILE_FOR_MOBILE..."
    else
        EXIT_CODE=1
        message $EXIT_CODE "Get the file to sync." "Pass me an argument or make sure cmus is running."
    fi
    notify-send -a $(basename "$0") -u $URGENCY -t $TIMEOUT "$MESSAGE"
    return $EXIT_CODE
}

function generateDestinationFilename()
{
    # delete everything until last /
    # @https://stackoverflow.com/a/16153529
    DEST_FILENAME=$(echo "${FILE_FOR_MOBILE##*/}")

    ARTIST=
    TITLE=
    EXTENSION=${FILE_FOR_MOBILE: -4}
    if [[ $EXTENSION == ".ogg" ]]; then
        ALBUMARTIST=$(vorbiscomment -l "$FILE_FOR_MOBILE" | grep -e '^ALBUMARTIST' | sed s/ALBUMARTIST=//)
        if [[ $(echo -n "$ALBUMARTIST" | wc -c) > 0 ]]; then
            ARTIST=$ALBUMARTIST
        else
            ARTIST=$(vorbiscomment -l "$FILE_FOR_MOBILE" | grep -e '^ARTIST' | sed s/ARTIST=//)
        fi
        TITLE=$(vorbiscomment -l "$FILE_FOR_MOBILE" | grep -e '^TITLE' | sed s/TITLE=//)
    elif [[ $EXTENSION == ".mp3" ]]; then
        ALBUMARTIST=$(ffprobe -loglevel error -show_entries format_tags=album_artist -of default=noprint_wrappers=1:nokey=1 "$FILE_FOR_MOBILE")
        if [[ $(echo -n "$ALBUMARTIST" | wc -c) > 0 ]]; then
            ARTIST=$ALBUMARTIST
        else
            ARTIST=$(ffprobe -loglevel error -show_entries format_tags=artist -of default=noprint_wrappers=1:nokey=1 "$FILE_FOR_MOBILE")
        fi
        TITLE=$(ffprobe -loglevel error -show_entries format_tags=title -of default=noprint_wrappers=1:nokey=1 "$FILE_FOR_MOBILE")
    fi
    if [[ $(echo -n $ARTIST | wc -c) > 0 ]]; then
        DEST_FILENAME="$ARTIST - $TITLE$EXTENSION"
    fi
    cleanString
    DEST_FILENAME=${DEST_FILENAME:0:100}
}

function cleanString()
{
    DEST_FILENAME=$(echo -n "$DEST_FILENAME" | sed "s/\//-/")
}

function copyToGoogleDrive()
{
    rclone copyto "$FILE_FOR_MOBILE" music_travel:Music/Travel/"$1"
    EXIT_CODE=$?
    message $EXIT_CODE "Copy $DEST_FILENAME to Google Drive."
    notify-send -a $(basename "$0") -u $URGENCY -t $TIMEOUT "$MESSAGE"
    return $EXIT_CODE
}

getFileForMobile
generateDestinationFilename
copyToGoogleDrive "$DEST_FILENAME"

exit $EXIT_CODE
