#!/bin/bash
# @TODO
#   - if first letter not [A-Z], put in folder "_"
#   - use list instead of individual condition $FILE == *.tar,*.zip...
#   - cp jpg, pdf, etc. won't work if this file comes before destination folders
#   have been set => need to "save" it for after LOSSLESS/Y_FOLDER have been set
#   - if missing tag, prompt user

shopt -s globstar

EXIT_CODE=0
MUSIC_ARCHIVE_FILE=
NEW_MUSIC_FOLDER=/home/didier/Music
TEMPORARY_FOLDER=$NEW_MUSIC_FOLDER/TempRaw
LOSSLESS_FOLDER=""
LOSSY_FOLDER=""
FILE_ARCHIVES_TYPES=(tar zip)
FILE_MUSIC_TYPES=(flac ogg)
FILE_COVER_TYPES=(jpg jpeg png pdf)
FILE_COUNT=0

convertFlacToOgg()
{
    RETURN_CODE=0
    EXIT_CODE_MAIN_FOLDER=0
    EXIT_CODE_SUBFOLDER=0
    echo "Convert flac to ogg"
    if compgen -G *.flac > /dev/null; then
        oggenc -q 6 *.flac
    fi
    EXIT_CODE_MAIN_FOLDER=$?
    # UPDATE: further tests in console give errors (probably special chars):
    # $ ls **/*.flac
    # ls: invalid option -- ' '
    # Try 'ls --help' for more information.
    # @TODO better off using find instead of relying on globstar
    # unable to glob on any depth, each solution I try work for one specific level
    # with shopt -s globstar in ~/.bashrc
    # ls **/*.flac works on any depth
    if compgen -G **/*.flac > /dev/null; then
        oggenc -q 6 **/*.flac
    fi
    EXIT_CODE_SUBFOLDER=$?
    if (( EXIT_CODE_MAIN_FOLDER > 0 && EXIT_CODE_SUBFOLDER > 0 )); then
        echo "EXIT_CODE_MAIN_FOLDER is $EXIT_CODE_MAIN_FOLDER"
        echo "EXIT_CODE_SUBFOLDER is $EXIT_CODE_SUBFOLDER"
        RETURN_CODE=1
    fi
    return $RETURN_CODE
}

# Read tags TRACKNUMBER and TITLE
# and copy $1 to path matching $2 file extension
# @param FILE the file to process
# @param EXTENSION lowercase without the initial dot
copyFile()
{
    FILE=$1
    EXTENSION=$2
    DEST_FOLDER=""
    if [[ $EXTENSION == 'flac' ]]; then
        TRACKNUMBER=$(metaflac --show-tag=tracknumber "$FILE" | cut -d '=' -f 2)
        TITLE=$(metaflac --show-tag=title "$FILE" | cut -d '=' -f 2)
        DEST_FOLDER=$LOSSLESS_FOLDER
    elif [[ $EXTENSION == 'ogg' ]]; then
        TRACKNUMBER=$(vorbiscomment --list "$FILE" | grep -i TRACKNUMBER | cut -d '=' -f 2)
        TITLE=$(vorbiscomment --list "$FILE" | grep -i TITLE | cut -d '=' -f 2)
        DEST_FOLDER=$LOSSY_FOLDER
    fi
    # bc evaluates the expression between quotes
    if [[ $(bc <<< "$TRACKNUMBER < 10") == 1 ]]; then
        TRACKNUMBER="0$TRACKNUMBER"
    fi
    echo "TRACKNUMBER is $TRACKNUMBER"
    # delete characters which do not play nice with filenames
    TITLE=$(echo "$TITLE" | sed 's|[!/]|_|g')
    echo "TITLE is $TITLE"
    FILENAME="${TRACKNUMBER}-${TITLE}.$EXTENSION"
    echo "New filename is $FILENAME"
    echo "Copying $FILE to $DEST_FOLDER/$FILENAME"
    cp "$FILE" "$DEST_FOLDER/$FILENAME"
}

createTempFolder()
{
    echo "Create temporary folder $TEMPORARY_FOLDER"
    rm -rf $TEMPORARY_FOLDER
    mkdir $TEMPORARY_FOLDER
}

displayUsage()
{
    echo "Usage:"
    echo "cd $NEW_MUSIC_FOLDER"
    echo "$0 <archive.tar|zip>"
    exit 1
}

extractArchive()
{
    echo "This is a $MUSIC_ARCHIVE_FILE_TYPE archive, extracting..."
    if [[ $MUSIC_ARCHIVE_FILE_TYPE == 'tar' ]]; then
        tar -xvf "$NEW_MUSIC_FOLDER/$MUSIC_ARCHIVE_FILE"
    elif [[ $MUSIC_ARCHIVE_FILE_TYPE == 'zip' ]]; then
        unzip "$NEW_MUSIC_FOLDER/$MUSIC_ARCHIVE_FILE"
    fi
}

incrementFileCount()
{
    FILE_COUNT=$(($FILE_COUNT+1))
}

if [[ $# -ne 1 || ( $1 != *.tar && $1 != *.zip ) ]]; then
    displayUsage
else
    MUSIC_ARCHIVE_FILE=$1
    if [[ $1 == *.tar ]]; then
        MUSIC_ARCHIVE_FILE_TYPE="tar"
    elif [[ $1 == *.zip ]]; then
        MUSIC_ARCHIVE_FILE_TYPE="zip"
    fi
fi

if [[ $(pwd) != $NEW_MUSIC_FOLDER ]]; then
    displayUsage
fi

cd $NEW_MUSIC_FOLDER
createTempFolder
echo "Copy $NEW_MUSIC_FOLDER/$MUSIC_ARCHIVE_FILE to $TEMPORARY_FOLDER"
cp "$NEW_MUSIC_FOLDER/$MUSIC_ARCHIVE_FILE" $TEMPORARY_FOLDER/
cd $TEMPORARY_FOLDER
pwd
extractArchive
convertFlacToOgg
EXIT_CODE=$?
if [[ $EXIT_CODE > 0 ]]; then
    echo "ERROR returned by oggenc !"
    exit $EXIT_CODE
fi
FOLDERS=$(find . -type d)
for FOLDER in $FOLDERS; do
    cd $FOLDER
    pwd
    for FILE_TYPE in "${FILE_MUSIC_TYPES[@]}"; do
        # @source https://stackoverflow.com/questions/301039/how-can-i-escape-white-space-in-a-bash-loop-list#301059
        while IFS= read -r -d '' FILE; do
            echo "Processing $FILE";
            if [[ "$FILE" == *.flac ]]; then
                echo "This is a FLAC file."
                if [[ $FILE_COUNT == 0 ]]; then
                    ARTIST=$(metaflac --show-tag=albumartist "$FILE")
                    if [[ "$ARTIST" == '' ]]; then
                        ARTIST=$(metaflac --show-tag=artist "$FILE")
                    fi
                    ARTIST=$(echo $ARTIST | cut -d '=' -f 2)
                    echo "ARTIST is $ARTIST"
                    YEAR=$(metaflac --show-tag=date "$FILE")
                    YEAR=$(echo $YEAR | cut -d '=' -f 2)
                    echo "YEAR is $YEAR"
                    ALBUM=$(metaflac --show-tag=album "$FILE")
                    ALBUM=$(echo $ALBUM | cut -d '=' -f 2)
                    echo "ALBUM is $ALBUM"
                    FIRST_LETTER=$(echo $ARTIST | cut -b 1 | tr [:lower:] [:upper:])
                    echo "FIRST_LETTER is $FIRST_LETTER"
                    LOSSLESS_FOLDER="$NEW_MUSIC_FOLDER/Lossless/$FIRST_LETTER/$ARTIST/${YEAR}-${ALBUM}"
                    echo "Creating folder $LOSSLESS_FOLDER"
                    mkdir -p "$LOSSLESS_FOLDER"
                    LOSSY_FOLDER="$NEW_MUSIC_FOLDER/Lossy/$FIRST_LETTER/$ARTIST/${YEAR}-${ALBUM}"
                    echo "Creating folder $LOSSY_FOLDER"
                    mkdir -p "$LOSSY_FOLDER"
                fi
                copyFile "$FILE" 'flac'
                incrementFileCount
            elif [[ "$FILE" == *.ogg ]]; then
                echo "This is a OGG file."
                copyFile "$FILE" 'ogg'
                incrementFileCount
            fi
        done < <(find . -iname "*.$FILE_TYPE" -print0)
    done
    for FILE_COVER_TYPE in "${FILE_COVER_TYPES[@]}"; do
        while IFS= read -r -d '' FILE; do
            echo "Copying $FILE in $LOSSLESS_FOLDER"
            cp "$FILE" "$LOSSLESS_FOLDER/"
            echo "Copying $FILE in $LOSSY_FOLDER"
            cp "$FILE" "$LOSSY_FOLDER/"
        done < <(find . -iname "*.$FILE_COVER_TYPE" -print0)
    done
    cd $TEMPORARY_FOLDER
done

cd ..
rm -rf $TEMPORARY_FOLDER

exit $EXIT_CODE
