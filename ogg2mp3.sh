#!/bin/bash
# https://gordonlesti.com/linux-convert-all-ogg-files-in-a-directory-to-mp3/

FOLDER_MP3=/home/didier/Music/mp3
ARTIST=
ALBUM=
FILE=

function makeFolders()
{
    if [[ -z "$ARTIST" ]]; then
        FILE=$1
        echo "--- Reading tags for $FILE ---"
        ARTIST=$(vorbiscomment --list "$FILE" | grep -ie "^ARTIST=" | sed "s/ARTIST=\(.*\)/\1/g")
        ALBUM=$(vorbiscomment --list "$FILE" | grep -ie "^ALBUM=" | sed "s/ALBUM=\(.*\)/\1/g")
        echo "Artist: $ARTIST"
        echo "Album: $ALBUM"
        echo "--- Creating $FOLDER_MP3/$ARTIST/$ALBUM ---"
        mkdir -p "$FOLDER_MP3/$ARTIST/$ALBUM"
    fi
}

find . -name "*.ogg" | while read line; do
    makeFolders "${line}"
    ffmpeg -i "$line" -acodec libmp3lame -map_metadata 0:s:0 "$FOLDER_MP3/$ARTIST/$ALBUM/${line:0:(-3)}mp3" </dev/null
done

exit 0
